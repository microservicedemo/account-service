FROM openjdk:8-jdk-alpine
COPY target/account-0.0.1-SNAPSHOT.jar accountservice.jar
ENTRYPOINT ["java", "-jar", "accountservice.jar"]