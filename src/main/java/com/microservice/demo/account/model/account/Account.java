package com.microservice.demo.account.model.account;

import com.microservice.demo.account.model.transaction.Transaction;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Entity
public class Account {


    @Id
    @GeneratedValue
    Long id;
    String accountNumber;
    Long currentBalance;
    @OneToMany(targetEntity = Transaction.class)
    List<Transaction> transactionList;
    String description;
}
