package com.microservice.demo.account.repository.transaction;

import com.microservice.demo.account.model.transaction.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {
    List<Transaction> findByCustomerId(Long customerId);
}
