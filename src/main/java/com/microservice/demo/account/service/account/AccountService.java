package com.microservice.demo.account.service.account;

import com.microservice.demo.account.model.account.Account;
import com.microservice.demo.account.repository.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {
    @Autowired
    AccountRepository accountRepository;

    public Account save(Account account) {
        return accountRepository.save(account);
    }

    public List<Account> findAll() {
        ArrayList<Account> accounts = new ArrayList<>();
        Iterable<Account> accountIterable = accountRepository.findAll();
        accountIterable.forEach(accounts::add);
        return accounts;
    }

    public Account findOne(Long id) {
        return accountRepository.findById(id).get();
    }

    public void delete(Long id) {
        Account account = new Account();
        account.setId(id);
        accountRepository.delete(account);
    }
}
