package com.microservice.demo.account.service.transaction;

import com.microservice.demo.account.model.transaction.Transaction;
import com.microservice.demo.account.repository.transaction.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    public Transaction save(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    public List<Transaction> findAll() {
        ArrayList<Transaction> accounts = new ArrayList<>();
        Iterable<Transaction> accountIterable = transactionRepository.findAll();
        accountIterable.forEach(accounts::add);
        return accounts;
    }

    public List<Transaction> findAllByCustomer(Long customerId) {
        ArrayList<Transaction> accounts = new ArrayList<>();
        Iterable<Transaction> accountIterable = transactionRepository.findByCustomerId(customerId);
        accountIterable.forEach(accounts::add);
        return accounts;
    }

    public Transaction findOne(Long id) {
        return transactionRepository.findById(id).get();
    }

    public void delete(Long id) {
        Transaction account = new Transaction();
        account.setId(id);
        transactionRepository.delete(account);
    }

}
