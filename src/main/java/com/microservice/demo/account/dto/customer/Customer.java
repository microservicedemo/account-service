package com.microservice.demo.account.dto.customer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Customer {
    Long id;
    String name;
    int age;
}
