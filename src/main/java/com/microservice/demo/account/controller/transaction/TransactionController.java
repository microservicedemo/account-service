package com.microservice.demo.account.controller.transaction;

import com.microservice.demo.account.model.transaction.Transaction;
import com.microservice.demo.account.service.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("acc-api/transactions")
@CrossOrigin(origins = "*")
public class TransactionController {
    @Autowired
    TransactionService transactionService;

    @RequestMapping(method= RequestMethod.POST)
    public Transaction save(@RequestBody Transaction account) {
        return transactionService.save(account);
    }

    @RequestMapping(method=RequestMethod.GET)
    public List<Transaction> findAll() {
        return transactionService.findAll();
    }

    @RequestMapping(value = "/customer/{customerId}", method=RequestMethod.GET)
    public List<Transaction> findAllByCustomer(@PathVariable("customerId") Long customerId) {
        return transactionService.findAllByCustomer(customerId);
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.GET)
    public Transaction findOne(@PathVariable("id") Long id) {
        return transactionService.findOne(id);
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        try {
            transactionService.delete(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(500).build();
        }
    }

}
