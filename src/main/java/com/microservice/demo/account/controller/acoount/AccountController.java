package com.microservice.demo.account.controller.acoount;


import com.microservice.demo.account.model.account.Account;
import com.microservice.demo.account.service.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/acc-api/accounts")
@CrossOrigin(origins = "*")
public class AccountController {

    @Autowired
    AccountService accountService;

    @RequestMapping(method=RequestMethod.POST)
    public Account save(@RequestBody Account account) {
        return accountService.save(account);
    }

    @RequestMapping(method=RequestMethod.GET)
    public List<Account> findAll() {
        return accountService.findAll();
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.GET)
    public Account findOne(@PathVariable("id") Long id) {
        return accountService.findOne(id);
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        try {
            accountService.delete(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(500).build();
        }
    }


}
